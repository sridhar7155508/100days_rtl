module tb;
  logic clk;
  logic reset;
  logic[3:0] lfsr_o;
  
  day7 DUT(.*);
  
  always #5 clk = ~clk;
  
  initial begin
    clk <=1'b1;
    reset <= 1'b1;
    @(posedge clk);
    @(posedge clk);
    reset <= 1'b0;
    repeat(10) @(posedge clk);
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
