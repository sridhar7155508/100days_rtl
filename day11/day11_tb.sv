module tb;
  
  logic clk;
  logic reset;
  
  logic[3:0] parallel_i;
  
  logic serial_o;
  logic empty_o;
  logic valid_o;
  
  day11 DUT(.*);
  
  always begin
    clk = 1'b1;
    #5;
    clk = ~clk;
    #5;
  end
  
  initial begin
    reset <= 1'b1;
    parallel_i <= 4'h0;
    @(posedge clk);
    @(posedge clk);    
    reset <= 1'b0;
    @(posedge clk);
    for(int i=0; i<32; i++) begin
      parallel_i <= $urandom_range(0, 4'hf);
      @(posedge clk);
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
