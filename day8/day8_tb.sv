// Code your testbench here
// or browse Examples
module tb;
  
  localparam BIN_W = 4;
  localparam ONE_HOT_W = 16;
  logic[BIN_W-1:0] bin_i;
  logic[ONE_HOT_W-1:0] one_hot_o;
  
  day8 DUT(.*);
  
  initial begin
//     for(int i=0; i<5; i++) begin
//       bin_i = $urandom_range(0, 4'hF);
//       #5;
//     end
    bin_i = 4'b1;
    #5;
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
