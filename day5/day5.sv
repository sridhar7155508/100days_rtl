module day5(
  input		logic		clk,
  input 	logic		reset,
  
  output	logic[7:0]	cnt_o
);
  
  logic[7:0] nxt_cnt;
  
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      cnt_o <= 8'h1;
    else 
      cnt_o <= nxt_cnt;
  end
      
  assign nxt_cnt = cnt_o + 8'h2;   
  
endmodule
