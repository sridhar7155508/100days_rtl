module tb;
  
  logic clk = 0;
  logic reset;
  
  logic[7:0] cnt_o;
  
  day5 DUT(.*);
  
  always #5 clk = ~clk;
  
  initial begin
    reset = 1'b1;
    @(posedge clk);
    @(posedge clk);
    reset = 1'b0;
    for(int i=0; i<10; i++)
      @(posedge clk);
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
endmodule
