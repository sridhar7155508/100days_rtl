module day1_tb();
  reg[7:0]	a_i;
  reg[7:0]	b_i;
  reg		s_i;
  wire[7:0]	y_o;
  
  day1 DUT(.*);
  
  initial begin
    for(int i=0; i<8; i++) begin
      a_i = $urandom_range(0, 8'hff);
      b_i = $urandom_range(0, 8'hff);
      s_i = $random%2;
      #5;
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
endmodule

