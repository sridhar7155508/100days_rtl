// Code your testbench here
// or browse Examples
module tb;
  
  logic clk;
  logic reset;
  
  logic load_i;
  logic[3:0] load_value_i;
  logic[3:0] count_o;
  
  int cycles;
  
  day10 DUT(.*);
  
  always begin
    clk = 1'b1;
    #5;
    clk = 1'b0;
    #5;
  end
  
  initial begin 
  	reset <= 1'b1;
    load_i <= 1'b0;
    load_value_i <= 4'h0;
    @(posedge clk);
    @(posedge clk);
    reset <= 1'b0;
    for(int i = 0; i<3; i++) begin
      load_i <= 'h1;
      load_value_i <= 3*i;
      cycles = 'hf - load_value_i[3:0];
      @(posedge clk);
      load_i <= 'h0;
      while(cycles) begin
        @(posedge clk);
        cycles--;
      end
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
