//Loadable counter
module day10(
  input		wire		clk,
  input		wire		reset,
  
  input		wire		load_i,
  input		wire[3:0]	load_value_i,
  
  output	wire[3:0]	count_o
);
  
  logic[3:0] load_ff;
  
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      load_ff <= 4'h0;
    else if(load_i)
      load_ff <= load_value_i;
  end
  
  logic[3:0] count_ff;
  logic[3:0] nxt_count;
  
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      count_ff <= 4'h0;
    else
      count_ff <= nxt_count;
  end
  
  assign nxt_count = (load_i) ? load_value_i : (count_ff == 4'hf) ? load_ff : count_ff + 1'b1;
  
  assign count_o = count_ff;
  
endmodule
