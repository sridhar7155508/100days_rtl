//driver sequence using get bext item
`include "uvm_macros.svh"

import uvm_pkg::*;

class day58_data extends uvm_sequence_item;
    
    `uvm_object_utils(day58_data)
    rand int data;

    function new(string name = "day58_data");
        super.new(name);
    endfunction

endclass


//sequence
class day58_seq extends uvm_sequence #(day58_data);

    `uvm_object_utils(day58_seq)

    function new(string name = "day58_seq");
        super.new(name);
    endfunction

    virtual task body();
        for(int i=0; i<30; i++) begin
            day58_data rand_data = day58_data::type_id::create("rand_data");
            rand_data.randomize();

            `uvm_info("SEQ", "Starting to send item", UVM_LOW)
            start_item(rand_data);
            finish_item(rand_data);
            `uvm_info("SEQ", "After finish item", UVM_LOW)
            
        end
    endtask

endclass

//driver
class day58_drv extends uvm_driver;

    `uvm_component_utils(day58_drv)

    function new(string name = "day58_drv", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual task run_phase(uvm_phase phase);
        super.run_phase(phase);
        `uvm_info("DRIVER", "Getting next item from sequencer", UVM_LOW)
        seq_item_port.get_next_item();
        `uvm_info("", $sformatf("Got the following data:ox%8x", req.data), UVM_LOW)
        #5;
        seq_item_port.item_done();
    endtask

endclass

//test
class day58_test extends uvm_test;

    `uvm_component_utils(day58_test)

    day58_seq day58_seq_h;
    day58_drv day58_drv_h;
    uvm_sequencer #(data58_data) uvm_sequencer_h;

    function new(string name = "day58_test", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        day58_drv_h = day58_drv::type_id::create("day58_drv_h", this);
        uvm_sequencer_h = uvm_sequencer #(day58_data)::type_id::create("uvm_sequencer_h", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        day58_drv_h.seq_item_port.connect(uvm_sequencer_h.seq_item_export);
    endfunction

    virtual task run_phase(uvm_phase phase);
        super.run_phase(phase);
        day58_seq_h = day58_seq::type_id::create("day58_seq_h");
        day58_seq_h.start(uvm_sequencer_h);
        #200;
    endtask

endclass

module top;

    initial begin
        run_test("data58_test");
    end
    
endmodule
