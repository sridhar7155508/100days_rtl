//bin to grey
module day9 #(
  parameter DATA_W = 4
)(
  input		wire[DATA_W-1:0]		bin_i,
  
  output	wire[DATA_W-1:0]		gray_o
);
  
  assign gray_o[DATA_W-1] = bin_i[DATA_W-1];
  
  genvar i;
  for(i=DATA_W-2; i>=0; i--) begin
    assign gray_o[i] = bin_i[i+1] ^ bin_i[i];
  end
  
endmodule
