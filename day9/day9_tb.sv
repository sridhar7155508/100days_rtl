// Code your testbench here
// or browse Examples
module tb;
  localparam DATA_W = 4;
  
  logic[DATA_W-1:0]		bin_i; 
  logic[DATA_W-1:0]		gray_o;
  
  day9 DUT(.*);
  
  initial begin
    for(int i=0; i<8; i++) begin
      bin_i <= $urandom_range(0, 4'hf);
      #5;
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
