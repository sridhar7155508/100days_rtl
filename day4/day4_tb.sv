module tb;
  
  logic[7:0] a_i;
  logic[7:0] b_i;
  logic[2:0] op_i;
  logic[7:0] alu_o;
  
  day4 DUT(.*);
  
  initial begin
    for(int i=0; i<3; i++) begin
      for(int j=0; j<=7; j++) begin
        a_i = $urandom_range(0, 8'hff);
        b_i = $urandom_range(0, 8'hff);
        op_i = 3'(j);
        #5;
      end
    end
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
