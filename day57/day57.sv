`include "uvm_macros.svh"

import uvm_pkg::*;

class sender extends uvm_component;
  
  `uvm_component_utils(sender)
  
  uvm_analysis_port #(int) sender_ap;
  
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    sender_ap = new("analysis_port", this);
  endfunction
  
  virtual task run_phase(uvm_phase phase);
    int rand_num;
    
    for(int i=0; i<100; i++) begin
      rand_num = $urandom_range(0, 100);
      sender_ap.write(rand_num);
      //if(rand_num == 30) 
        //break;
    end
    
  endtask
  
endclass

class sub1 #(type T = int) extends uvm_subscriber #(T);
  
  `uvm_component_utils(sub1)
  
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction
  
  virtual function void write(T t);
    
    string sub1_print;
    
    sub1_print = $sformatf("Got a new transaction. %3d", t);
    
    `uvm_info("SUB1", sub1_print, UVM_NONE);    
    
  endfunction
  
endclass

class sub2 #(type T = int) extends uvm_subscriber #(T);
  
  `uvm_component_utils(sub2)
  
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction
  
  virtual function void write(T t);
    
    string sub2_print;
    
    sub2_print = $sformatf("Got a new transaction. %3d", t);
    
    `uvm_info("SUB2", sub2_print, UVM_NONE);    
    
  endfunction
  
endclass

class day57_test extends uvm_test;
  
  `uvm_component_utils(day57_test)
  
  sender SEND;
  sub1 SUB1;
  sub2 SUB2;
  
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    SEND = sender::type_id::create("SEND", this);
    SUB1 = sub1#(int)::type_id::create("SUB1", this);
    SUB2 = sub2#(int)::type_id::create("SUB2", this);
  endfunction
  
  virtual function void connect_phase(uvm_phase phase);
    SEND.sender_ap.connect(SUB1.analysis_export);
    SEND.sender_ap.connect(SUB2.analysis_export);
  endfunction
  
endclass

module day57_tb;
  
  initial begin
    run_test("day57_test");
  end
  
endmodule
