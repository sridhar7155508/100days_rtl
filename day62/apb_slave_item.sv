`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_slave_item extends uvm_sequence_item;
    `uvm_object_utils(apb_slave_item)

    function new(string name = "apb_slave_item");
        super.new(name);
    endfunction

    function string tx2string();
        string tx;
        tx = $sformatf("pready = %0b, prdata = %0x", pready, prdata);
        return tx;
    endfunction

endclass
