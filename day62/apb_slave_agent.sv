`include "uvm_macros.svh"
import uvm_pkg::*;

class apb_slave_agent extends uvm_agent;

    `uvm_component_utils(apb_slave_agent)
    apb_slave_driver apb_slave_driver_h;
    apb_slave_monitor apb_slave_monitor_h;
    uvm_seqquencer #(apb_slave_item) uvm_seqquencer_h;

    function new(string name = "apb_slave_agent", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        uvm_seqquencer_h = uvm_seqquencer #(apb_slave_item)::type_id::create("uvm_sequencer_h", this);
        apb_slave_driver_h = apb_slave_driver::type_id::create("apb_slave_driver_h", this);
        apb_slave_monitor_h = apb_slave_monitor_h::type_id::create("apb_slave_monitor_h", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
        super.connect_phase(phase);
        apb_slave_driver_h.seq_item_port.connect(uvm_seqquencer_h.seq_item_export);
    endfunction

endclass
