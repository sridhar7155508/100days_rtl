`include "uvm_macros.svh"
import uvm_pkg::*;

class apb_slave_monitor extends uvm_monitor;
    `uvm_component_utils(apb_slave_monitor)
    virtual apb_slave_if vif;

    uvm_analysis_port #(apb_slave_item) uvm_analysis_port_h;

    function new(string name = "apb_slave_monitor", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db #(virtual apb_slave_if)::get(this, "", "apb_slave_vif", vif))
            `uvm_fatal("MON", "Could not find the handle for interface")

    endfunction

    virtual task run_phase(uvm_phase phase);

        super.run_phase(phase);
        forever begin
            @(posedge vif.clk);
            if(vif.psel & vif.penable & vif.pready) begin
                apb_slave_item item = new();
                item.prdata = vif.prdata;
                item.pwrite = vif.pwrite;
                item.pwdata = vif.pwdata;
                //Broadcasting to the subscribers
                uvm_analysis_port_h.write(item);
            end

        end

    endtask

endclass
