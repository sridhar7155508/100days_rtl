// Code your testbench here
// or browse Examples
module tb;
  logic		clk;
  logic		reset;
  logic		a_i;
  
  logic		rising_edge_o;
  logic		falling_edge_o;
  
  integer i;
  
  day3 DUT(.*);
  
  always begin
    clk = 1'b1;
    #5;
    clk = 1'b0;
    #5;
  end
  
  initial begin
    reset <= 1'b1;
    a_i <= 1;
    @(posedge clk);
    reset <= 1'b0;
    @(posedge clk);
    for( i=0; i<10; i++) begin
      a_i <= $random%2;
      @(posedge clk);
    end
    $finish();
  end
  
  initial begin
    $dumpfile("d.vcd");
    $dumpvars();
  end  
endmodule
