// seq_detector
module day12(
  input		wire	clk,
  input		wire	reset,
  
  input		wire	x_i,
  
  output	wire	det_o
);
  
  logic[11:0] shift_ff;
  logic[11:0] nxt_shift;
  
  always_ff @(posedge clk or posedge reset) begin
    if(reset)
      shift_ff <= 12'd0;
    else
      shift_ff <= nxt_shift;
  end
  
  assign nxt_shift = {x_i, shift_ff[11:1]};
  
  assign det_o = (shift_ff == 12'b1110_0111_0110);
  
endmodule
