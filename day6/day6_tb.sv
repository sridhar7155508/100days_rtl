// Code your testbench here
// or browse Examples
module tb;
  logic clk = 0;
  logic reset;
  
  logic x_i;
  
  logic[3:0] sr_o;
  
  day6 DUT(.*);
  
  always #5 clk = ~clk;
  
  initial begin
    reset <= 1'b1;
    x_i <= 1'b1;
    @(posedge clk);
    reset <= 1'b0;
    @(posedge clk);
    for(int i=0; i<10; i++) begin
      x_i <= $random%2;
      @(posedge clk);
    end
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
  end
  
endmodule
