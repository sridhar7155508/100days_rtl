// Code your testbench here
// or browse Examples
module tb();
  reg clk;
  reg rst;
  reg d_i;
  wire q_norst_o;
  wire q_syncrst_o;
  wire q_asyncrst_o;
  
  day2 DUT(.*);
  
//   always begin
//     clk = 1'b0;
//     #5;
//     clk = 1'b1;
//     #5;
//   end
  always #5 clk = ~clk;
  initial begin
    clk = 1;  
    rst = 1'b1;
    d_i = 1'b0;
    @(posedge clk);
    rst = 1'b0;
    @(posedge clk);
    d_i = 1'b1;
    @(posedge clk);
    @(posedge clk);
    @(negedge clk);
    rst = 1'b1;
    @(posedge clk);
    @(posedge clk);
    rst = 1'b0;
    @(posedge clk);
    @(posedge clk);
    $finish();
  end
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars();
    #1000 $finish();
  end
  
endmodule
