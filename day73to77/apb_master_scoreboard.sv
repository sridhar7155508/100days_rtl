`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_master_scoreboard uvm_scoreboard;

    `uvm_component_utils(apb_master_scoreboard)

    //Associate array
    bit[31:0] mem [bit[7:0]];
    uvm_analysis_imp #(apb_master_item, apb_master_scoreboard)  m_analysis_imp;

    function new(string name = "apb_master_scoreboard", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual function build_phase(uvm_phase phase);
        super.build_phase(phase);
        m_analysis_imp = new("m_analysis_imp", this);
    endfunction

    //Implement the write function
    virtual function write(apb_master_item item);
        logic[31:0] mem_data;
        `uvm_info("SCOREBOARD", "Got a new transaction", UVM_LOW)
        //write into a memory on write
        if(item.psel & item.penable & item.pwrite & item.pready) begin
            mem[item.paddr] = item.pwdata;
        end

        //Read from memory on a read
        if(item.psel & item.penable & ~item.pwrite & item.pready) begin
            mem_data = mem[item.paddr];
            if(mem_data != item.prdata) begin
                `uvm_fatal(get_type_name(), $sformatf("APB slave read data does not match. Expected: 0x%8x Got: 0x%8x", mem_data, item.prdata))
        end
    endfunction

endclass
