`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_master_raw_seq extends uvm_sequence;

    `uvm_object_utils(apb_master_raw_seq)

    //Number of transactions to be sent
    rand int num_txn;

    //store the write addr for next read
    bit[31:0] wr_addr[$];

    function new(string name = "apb_master_raw_seq");
        super.new(name);
    endfunction

    
endclass
