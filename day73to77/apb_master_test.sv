`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_master_test extends uvm_test;

    `uvm_component_utils(apb_master_test)

    apb_master_env          e0;
    virtual apb_master_if   vif;

    function new(string name = "apb_master_test", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        e0 = apb_master_env::type_id::create("e0", this);
        //Get the virtual interface handle from config db
        if(!(uvm_config_db #(virtual apb_master_if) :: get(this, "", "apb_master_intf", vif) )) begin
            `uvm_fatal(get_type_name(), "Could not get the interface handle")
        end
        //set the interface in config db
        uvm_config_db #(virtual apb_master_if) :: set(this, "e0.a0.*","apb_master_intf", vif );
    endfunction

    //start the sequence in run_phase
    task run_phase(uvm_phase phase);
        apb_master_raw_seq seq = apb_master_raw_seq::type_id::create("seq", this);
        phase.raise_objection(this);
            seq.randomize();
            @(negedge vif.reset);
            seq.start(e0.a0.s0);
        phase.drop_objection(this);
    endtask

endclass
