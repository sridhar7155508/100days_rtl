include "uvm_macros.svh"

import uvm_pkg::*;

class apb_master_driver extends uvm_driver;

    `uvm_component_utils(apb_master_driver)

    virtual apb_master_if vif;

    function new(string name = "apb_master_driver", uvm_component parent);
        super.new(name, parent);
    endfunction

    //Get the virtaul interface handle in the build phase
    virtual function build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db #(apb_master_item) :: get(this, "", "apb_master_intf", vif)) begin
            `uvm_fatal("DRIVER", "Could not get the handle for interface")
        end
    endfunction

    //Drive the transaction in run phase
    virtual task run_phase(uvm_phase phase);
        super.run_phase(phase);
        //Always try to get the transaction from the sequencer
        forever begin
            apb_master_item d_item;
            `uvm_info("DRIVER", "Waiting for the item from the sequencer", UVM_LOW)
            seq_item_port.get_next_item(d_item);
                //Drive the sequence item on to the RTL ports
                //Drive psel and penable as per the APB protocol
                //Once the sequences sets the psel, drive the psel and penable with the
                //randomized payload
                vif.psel <= 1'b0;
                vif.penable <= 1'b0;
                if(d_item.psel) begin
                    vif.psel <= d_item.psel;
                    @(vif.cb);
                    vif.penable <= 1;
                    vif.paddr <= d_item.paddr;
                    vif.pwrite <= d_item.pwrite;
                    vif.pwdata <= d_item.pwdata;
                    //Monitor the pready
                    forever begin
                        @(vif.cb);
                        if(vif.pready) begin
                            @(vif.cb);
                            vif.psel <= 1'b0;
                            vif.penable <= 1'b0;
                            break;
                        end
                    end
                end
                @(vif.cb);
            seq_item_port.item_done(d_item);
        end
    endtask

endclass
