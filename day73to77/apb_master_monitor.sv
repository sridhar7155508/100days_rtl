`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_master_monitor extends uvm_monitor;

    `uvm_component_utils(apb_master_monitor)

    virtual apb_master_if vif;
    uvm_analysis_port #(apb_master_item) mon_analysis_Port;

    function new(string name = "apb_master_monitr", uvm_component pasrent);
        super.new(name, parent);
    endfunction

    //Get the virtual interface handle in the build phase
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        mon_analysis_Port = new(this, "mon_analysis_port");
        if(!(uvm_config_db #(apb_master_item) :: get(this, "", "apb_master_intf", vif))) begin
            `uvm_fatal("DRIVER", "Could not find the interface handle")
        end
    endfunction

    //MOnitor the interface in run phase
    virtual task run_phase(uvm_phase phase);
        apb_master_item item = new;
        super.run_phase(phase);
        forever begin
            item.psel = vif.cb.psel;
            item.penable = vif.cb.penable;
            item.paddr = vif.cb.paddr;
            item.pwrite = vif.cb.pwrite;
            item.pwdata = vif.cb.pwdata;
            item.pready = vif.cb.pready;
            item.prdata = vif.cb.prdata;
            `uvm_info(get_type_name(), item.tx2string, UVM_LOW)
            //Broadcast to all the subscriber classes
            mon_analysis_Port.write(item);
            @(vif.cb);
        end
    endtask

endclass
