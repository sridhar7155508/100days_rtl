`include "uvm_macros.svh"

import uvm_pkg::*;

class day61_drv extends uvm_driver;

    `uvm_component_utils(day61_drv)

    virtual day61_if vif;

    function new(string name = "day61_drv", uvm_component parent);
        super.new(name, parent);
    endfunction

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!uvm_config_db #(virtual day61_if)::get(this, "", "day61_vif", vif))
        `uvm_fatal("DRV", "Could not get the handle for the virtual interface")
    endfunction

    virtual task run_phase(uvm_phase phase);
        super.run_phase(phase);

        //Always try to get transaction from sequencer
        forever begin
            day61_item day61_item_h;
            `uvm_info("DRV", "Waiting for the next item from the sequencer", UVM_LOW)
            seq_item_port.get_next_item(day61_item_h);
            //
            seq_item_port.item_done(day61_item_h);
        end
    endtask

endclass
