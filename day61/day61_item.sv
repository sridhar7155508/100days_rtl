`include "uvm_macros.svh"

import uvm_pkg::*;

class day61_item extends uvm_sequence_item;

    `uvm_object_utils(day61_item)

    rand bit        pready;
    rand bit[31:0]  prdata;

    function new(string name = "day61_item");
        super.new(name);
    endfunction

    function string tx2string();
        string tx;
        tx = $sformatf("pready = %0b, prdata = %0x", pready, prdata);
        return tx;
    endfunction

endclass
