module apb_master(
    input       wire        clk,
    input       wire        reset,

    output      wire        psel_o,
    output      wire        penable_o,
    output      wire[9:0]   paddr_o,
    output      wire        pwrite_o,
    output      wire[31:0]  pwdata_o,

    input       wire        pready_i,
    input       wire[31:0]  prdata_i
);

    //Enum for the APB state
    typedef enum bit[1:0] {ST_IDLE = 2'b00, ST_SETUP = 2'b01, ST_ACCESS = 2'b10} apb_state_t;

    apb_state_t state_q;
    apb_state_t next_state;

    logic[31:0] rdata_q;

    //Load counter with LFSR value everytime a pready is seen
    //Wait for counter to be 0 before starting the new APB request
    //This removes the need of "cmd_i" signal for starting the request
    logic[3:0] count_ff;
    logic[3:0] next_count;
    logic[3:0] count_ff;
    logic[3:0] lfsr_val;

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            count_ff <= 4'hf;
        end
        else begin
            count_ff <= next_count;
        end
                    
    end

    assign next_count = penable_o & pready_i ? lfsr_val : count_ff - 4'h1;

    assign count = count_ff;

    //Generate a random load value
    day7 DAY7(
        .clk(clk),
        .reset(reset),

        .lfsr_o(lfsr_val)
    );

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            state_q <= ST_IDLE;
        end
        else begin
            state_q <= next_state;
        end
    end

    always_comb begin
        next_state = state_q;
        case(state_q) 
            ST_IDLE : 
                if(count == 4'd0) next_state = ST_SETUP; else next_state = ST_IDLE;
            ST_SETUP : next_state = ST_ACCESS;
            ST_ACCESS : 
                if(pready_i) next_state = ST_IDLE;
        endcase
    end

    logic ping_pong;

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            ping_pong <= 1'b1;
        end
        else if(state_q == ST_ACCESS) begin
            ping_pong <= ~ping_pong;
        end
    end

    assign psel_o       = ( (state_q == ST_SETUP) | (state_q == ST_ACCESS) );
    assign penable_o    = (state_q == ST_ACCESS);
    assign pwrite_o     = ping_pong;
    assign paddr_o      = 10'h3FE;
    assign pwdata_o     = rdata_q + 32'h1;

    //Capture the read data to store it for next write
    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            rdata_q <= 32'h0;
        end
        else if( penable_o & pready_i ) begin
            rdata_q <= prdata_i;
        end
    end

endmodule

//LFSR(Linear Feedback Shift Register)
module day7(
    input       wire        clk,
    input       wire        reset,

    output      wire[3:0]   lfsr_o
);
    logic[3:0] lfsr_ff;
    logic[3:0] nxt_lfsr;

    always_ff @(posedge clk or posedge reset) begin
        if(reset) begin
            lfsr_ff <= 4'he;
        end
        else begin
            lfsr_ff <= nxt_lfsr;
        end
    end

    assign nxt_lfsr = { lfsr_ff[2:0], (lfsr_ff[3]^lfsr_ff[1]) };

    assign lfsr_o = lfsr_ff;

endmodule
