`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_slave_basic_seq extends uvm_sequence;

    `uvm_object_utils(apb_slave_basic_seq)

    //Number of transactions to be sent
    int num_txns;

    function new(string name = "apb_slave_basic_seq");
        super.new(name);
    endfunction

    //allow anywhere between 1 to 20 txns
    constraint c1{ num_txns inside {[1:20]}; }

    //Generate the item in the body
    virtual task body();
        string tx;
        for(int i=0; i<num_txns; i++) begin
            apb_slave_item seq_item = apb_slave_item::type_id::create("seq_item");
            `uvm_info("SEQUENCE", "Starting a new APB slave item", UVM_LOW)
            start_item(seq_item);
            void'(seq_item.randomize());
            tx = seq_item.tx2string();
            `uvm_info("SEQUENCE", $sformatf("Generated a new APB slave item:\n%s", tx), UVM_LOW)
            finish_item(seq_item);
        end
        `uvm_info("SEQUENCE", "Done sending all the APB slave items", UVM_LOW)
    endtask

endclass
