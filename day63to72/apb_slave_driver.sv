`include "uvm_macros.svh"

import uvm_pkg::*;

class apb_slave_driver extends uvm_driver;

    `uvm_component_utils(apb_slave_driver)

    virtual apb_slave_if vif;

    function new(string name = "apb_slave_driver", uvm_component parent);
        super.new(name, parent);
    endfunction

    //Get the virtual interface pointer in the build phase from the config db
    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if(!(uvm_config_db #(virtual apb_slave_if) :: get(this, "", "apb_slave_vif", vif)))
            `uvm_fatal("DRV", "Could not able to get the interface handle")
    endfunction

    //drive the transaction in the run_phase
    virtual task run_phase(uvm_phase phase);
        super.run_phase(phase);
        //Always try to get a new transaction from the sequence
        forever begin
            apb_slave_item d_item;
            `uvm_info("DRV", "Waiting to get the item from the sequencer", UVM_LOW)     
            seq_item_port.get_next_item(d_item);
            //drive the sequence item on the RTL ports
            //Only need to drive the pready and prdata signals
            vif.cb.pready <= d_item.pready;
            vif.cb.prdata <= d_item.prdata;
            @(vif.cb)
            //keep pready and prdata asserted until penable is seen
            if(d_item.pready) begin
                while(1) begin
                    if(vif.penable) break;
                    else @(vif.cb)
                end
            end
            seq_item_port.item_done();          
        end

    endtask

endclass
